# README #

NumbersAPIGame is a quiz game for iOS devices (iOS7 or higher) that works with [numbersapi.com](numbersapi.com).

### How To Get Started? ###

This project uses [AFNetworking framework](http://afnetworking.com) as the base of the connection layer.
To install this dependency you only have to open the console and navigate to the base directory of the project, then key this line:

```
#!shell
$ pod install

```

And open the" xcworkspace".

If you have any doubt, visit the [cocoapods site](cocoapods.org).