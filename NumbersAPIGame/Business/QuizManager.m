//
//  QuizManager.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "QuizManager.h"

#import "NSNumber+Random.h"
#import "NSMutableArray+Shuffle.h"
#import "NumbersAPISessionManager.h"
#import "Fact.h"

NSInteger const NumberOfAnswers         = 4;
NSInteger const NumberOfCorrectAnswers  = 1; //Only one correct answer for each question
NSInteger const RandomRangeFactor       = 4; //A coefficient to establish the range for the random numbers

@interface QuizManager ()

@property (strong, nonatomic) Fact *currentFact;
@property (strong, nonatomic) NSArray *currentAnswers;

@end

@implementation QuizManager

#pragma mark - Singleton

+ (instancetype)sharedManager {
    static QuizManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Question methods

- (void)nextQuestion:(QuestionObjectCompletionBlock)completionBlock {
    [[NumbersAPISessionManager sharedManager] getRandomFact:^(NSDictionary *factAttributes, NSError *error) {
        if (error) {
            completionBlock(nil, nil, error);
            return;
        }
        self.currentFact = [[Fact alloc] initWithAttributes:factAttributes];
        self.currentAnswers = [self answersForFact:self.currentFact];
        completionBlock(self.currentFact, self.currentAnswers, nil);
    }];
}

#pragma mark - Answers methods

- (NSArray *)answersForFact:(Fact *)fact {
    NSMutableArray *answers = [[NSMutableArray alloc] initWithCapacity:NumberOfAnswers];
    [answers addObject:[self rightAnswerForFact:fact]];
    [answers addObjectsFromArray:[self wrongAnswersForFact:fact]];
    [answers shuffleObjects];

    return answers;
}

- (NSNumber *)rightAnswerForFact:(Fact *)fact {
    return fact.number;
}

- (NSArray *)wrongAnswersForFact:(Fact *)fact {
    NSInteger numberOfWrongAnswers = NumberOfAnswers - NumberOfCorrectAnswers;
    NSMutableArray *wrongAnswers = [[NSMutableArray alloc] initWithCapacity:numberOfWrongAnswers];
    for (NSInteger i = 0; i < numberOfWrongAnswers; i++) {
        [wrongAnswers addObject:[self generateWrongAnswerForFact:fact]];
    }
    
    return wrongAnswers;
}

- (NSNumber *)generateWrongAnswerForFact:(Fact *)fact {    
    NSInteger minValue = [fact.number integerValue] / RandomRangeFactor;
    NSInteger maxValue = [fact.number integerValue] * RandomRangeFactor;
    NSNumber *random = [NSNumber randomNumberBetween:minValue and:maxValue];
    
    return random;
}

- (BOOL)checkIfAnswerIsCorrect:(NSNumber *)selectedAnswer {
    return [selectedAnswer compare:[self rightAnswerForFact:self.currentFact]] == NSOrderedSame;
}

@end
