//
//  QuizManager.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const NumberOfAnswers;

@class Fact;

typedef void (^QuestionObjectCompletionBlock)(Fact *fact, NSArray *answers, NSError *error);

@interface QuizManager : NSObject

+ (instancetype)sharedManager;

- (void)nextQuestion:(QuestionObjectCompletionBlock)completionBlock;
- (BOOL)checkIfAnswerIsCorrect:(NSNumber *)selectedAnswer;

@end
