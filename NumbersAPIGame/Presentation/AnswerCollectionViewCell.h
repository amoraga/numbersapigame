//
//  AnswerCollectionViewCell.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 06/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnswerCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

@end
