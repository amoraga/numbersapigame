//
//  AnswerCollectionViewCell.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 06/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "AnswerCollectionViewCell.h"

@implementation AnswerCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupSelectedBackgroundView];
}

- (void)setupSelectedBackgroundView {
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    selectedBackgroundView.backgroundColor = [UIColor lightGrayColor];
    self.selectedBackgroundView = selectedBackgroundView;
}

@end
