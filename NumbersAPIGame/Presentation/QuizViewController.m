//
//  QuizViewController.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "QuizViewController.h"
#import "UIViewController+StandardizedAlert.h"
#import "AnswerCollectionViewCell.h"
#import "QuizManager.h"
#import "Fact.h"

static NSInteger const LabelNumberOfLinesNoLimit = 0;

static NSString * const AnswerCollectionViewCellReuseIdentifier = @"AnswerCollectionViewCell";

@interface QuizViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;

@property (weak, nonatomic) Fact *fact;
@property (weak, nonatomic) NSArray *answers;

@end

@implementation QuizViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupQuestionLabel];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self setupAnswersCollectionViewFlowLayout];
    [self loadNextQuestion];
}

#pragma mark - User Interface

- (void)setupQuestionLabel {
    self.questionLabel.numberOfLines = LabelNumberOfLinesNoLimit;
}

- (void)setupAnswersCollectionViewFlowLayout {
    CGRect collectionViewFrame = self.collectionView.frame;
    CGFloat numberOfCells = NumberOfAnswers;
    CGFloat totalHeightForInterlines = self.collectionViewFlowLayout.minimumLineSpacing * numberOfCells;
    self.collectionViewFlowLayout.footerReferenceSize = CGSizeMake(collectionViewFrame.size.width, self.collectionViewFlowLayout.minimumLineSpacing);

    CGFloat totalHeightForAnswersCells = collectionViewFrame.size.height - totalHeightForInterlines - self.collectionViewFlowLayout.footerReferenceSize.height;
    CGFloat cellHeight = totalHeightForAnswersCells / numberOfCells;
    self.collectionViewFlowLayout.itemSize = CGSizeMake(collectionViewFrame.size.width, cellHeight);
}

- (void)updateQuizInterface {
    self.questionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Question template", nil), self.fact.text];
    [self.collectionView reloadData];
}

- (void)loadNextQuestion {
    self.view.userInteractionEnabled = NO;
    [[QuizManager sharedManager] nextQuestion:^(Fact *fact, NSArray *answers, NSError *error) {
        self.view.userInteractionEnabled = YES;
        if (error) {
            [self showAlertObjectWithTitle:NSLocalizedString(@"Error title", nil) message:NSLocalizedString(@"Error message", nil) cancelTitle:NSLocalizedString(@"Retry", nil) delegate:self handler:^(UIAlertAction *action) {
                [self loadNextQuestion];
            }];
            return;
        }
        self.fact = fact;
        self.answers = answers;
        [self updateQuizInterface];
    }];
}

- (void)showResultMessage:(BOOL)answerIsCorrect {
    NSString *resultTitle = answerIsCorrect ? NSLocalizedString(@"Right answer title", nil) : NSLocalizedString(@"Wrong answer title", nil);
    NSString *resultMessage = answerIsCorrect ? NSLocalizedString(@"Right answer", nil) : NSLocalizedString(@"Wrong answer", nil);
    
    [self showAlertObjectWithTitle:resultTitle message:resultMessage cancelTitle:NSLocalizedString(@"Next question", nil) delegate:self handler:^(UIAlertAction *action) {
        [self loadNextQuestion];
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self loadNextQuestion];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.answers count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AnswerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:AnswerCollectionViewCellReuseIdentifier forIndexPath:indexPath];
    cell.answerLabel.text = [[self.answers objectAtIndex:indexPath.row] stringValue];

    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *selectedAnswer = [self.answers objectAtIndex:indexPath.row];
    BOOL answerIsCorrect = [[QuizManager sharedManager] checkIfAnswerIsCorrect:selectedAnswer];
    [self showResultMessage:answerIsCorrect];
}

@end
