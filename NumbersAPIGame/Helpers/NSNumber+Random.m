//
//  NSNumber+Random.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 06/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "NSNumber+Random.h"

@implementation NSNumber (Random)

+ (NSNumber *)randomNumberBetween:(NSInteger)smallValue and:(NSInteger)bigValue {
    NSInteger range = bigValue - smallValue;
    NSInteger generatedRandom = arc4random_uniform((int32_t)range) + smallValue;
    
    return @(generatedRandom);
}

@end
