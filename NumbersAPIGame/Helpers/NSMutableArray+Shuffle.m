//
//  NSMutableArray+Shuffle.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 06/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "NSMutableArray+Shuffle.h"

@implementation NSMutableArray (Shuffle)

- (void)shuffleObjects {
    NSInteger numberOfObjects = self.count;
    for (NSInteger i = 0; i < numberOfObjects; i++) {
        NSInteger newRandomIndex = arc4random_uniform((int32_t)numberOfObjects);
        [self exchangeObjectAtIndex:i withObjectAtIndex:newRandomIndex];
    }
}

@end
