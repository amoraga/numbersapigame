//
//  NSMutableArray+Shuffle.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 06/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Shuffle)

- (void)shuffleObjects;

@end
