//
//  UIViewController+StandardizedAlert.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 07/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "UIViewController+StandardizedAlert.h"

@implementation UIViewController (StandardizedAlert)

///-----------------------------------
/// @name showAlertObjectWithTitle
///-----------------------------------

/**
 * This method shows a simple alert message with a single button
 *
 * @param the title of the alert
 * @param the message of the alert
 * @param the title of the single button
 * @param the delegate for the UIAlertView
 * @param the handler for the UIAlertController
 *
 * @discussion UIAlertView is deprecated in iOS8 and UIAlertController isn't available in iOS7.
 * This method provides a way to show a simple alert message without dealing with this fragmentation.
 *
 */

- (void)showAlertObjectWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle delegate:(id)delegate handler:(void (^)(UIAlertAction *action))handler {
    if ([UIAlertController class]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleDefault handler:handler];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelTitle otherButtonTitles:nil];
        [alertView show];
    }
}

@end
