//
//  NSNumber+Random.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 06/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Random)

+ (NSNumber *)randomNumberBetween:(NSInteger)smallValue and:(NSInteger)bigValue;

@end
