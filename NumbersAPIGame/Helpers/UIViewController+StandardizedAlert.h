//
//  UIViewController+StandardizedAlert.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 07/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (StandardizedAlert)

- (void)showAlertObjectWithTitle:(NSString *)title message:(NSString *)message cancelTitle:(NSString *)cancelTitle delegate:(id)delegate handler:(void (^)(UIAlertAction *action))handler;

@end
