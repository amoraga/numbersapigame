//
//  NumbersAPISessionManager.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "NumbersAPISessionManager.h"

static NSString * const NumbersAPIBaseURL           = @"http://numbersapi.com/";
static NSString * const NumbersAPIRandomRelativeURL = @"random";

static NSString * const HTTPHeadersContentTypeParam = @"content-Type";
static NSString * const HTTPHeadersJSONValue        = @"application/json";

static NSString * const NumbersAPIFragmentParam     = @"fragment";  //Formats the fact as a sentence fragment to include it as part of a larger sentence
static NSString * const NumbersAPINotFoundParam     = @"notfound";  //Describes the behavior when one fact isn't found
static NSString * const NumbersAPIMaxParam          = @"max";       //Sets the max possible value
static NSString * const NumbersAPIFloorValue        = @"floor";     //A 'notfound' param's value. Rounds down a number without associated fact

@implementation NumbersAPISessionManager

#pragma mark - Singleton

+ (instancetype)sharedManager {
    static NumbersAPISessionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Initializer

- (instancetype)init {
    self = [super initWithBaseURL:[self baseURL] sessionConfiguration:[self sessionConfiguration]];

    return self;
}

#pragma mark - SessionManager parameters

- (NSURL *)baseURL {
    return [NSURL URLWithString:NumbersAPIBaseURL];
}

- (NSURLSessionConfiguration *)sessionConfiguration {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{
                                                   HTTPHeadersContentTypeParam : HTTPHeadersJSONValue
                                                   };
    
    return sessionConfiguration;
}

#pragma mark - Facts webservice

- (void)getRandomFact:(FactDictionaryCompletionBlock)completionBlock {
    NSDictionary *parametersForRandomFact = @{
                                                      NumbersAPIFragmentParam    : [NSNull null],
                                                      NumbersAPINotFoundParam    : NumbersAPIFloorValue,
                                                      NumbersAPIMaxParam         : @(NSIntegerMax)
                                                      };

    [self GET:NumbersAPIRandomRelativeURL parameters:parametersForRandomFact success:^(NSURLSessionDataTask *task, id responseObject) {
        completionBlock(responseObject, nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        completionBlock(nil, error);
    }];
}

@end
