//
//  NumbersAPISessionManager.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "AFHTTPSessionManager.h"

typedef void (^FactDictionaryCompletionBlock)(NSDictionary *factAttributes, NSError *error);

@interface NumbersAPISessionManager : AFHTTPSessionManager

+ (instancetype)sharedManager;

- (void)getRandomFact:(FactDictionaryCompletionBlock)completionBlock;

@end
