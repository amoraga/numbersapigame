//
//  Fact.h
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fact : NSObject

@property (strong, nonatomic) NSNumber *number;
@property (strong, nonatomic) NSString *text;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end
