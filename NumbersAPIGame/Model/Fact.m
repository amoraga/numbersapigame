//
//  Fact.m
//  NumbersAPIGame
//
//  Created by Alberto Moraga on 05/10/14.
//  Copyright (c) 2014 AMA. All rights reserved.
//

#import "Fact.h"

static NSString * const NumbersAPINumberAttribute   = @"number";
static NSString * const NumbersAPITextAttribute     = @"text";

@implementation Fact

- (instancetype)initWithAttributes:(NSDictionary *)attributes {
    self = [super init];
    
    if (self) {
        _number = [attributes objectForKey:NumbersAPINumberAttribute];
        _text   = [attributes objectForKey:NumbersAPITextAttribute];
    }
    
    return self;
}

@end
